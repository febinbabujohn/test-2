public class Triangle extends Shape implements TwoDimensionalShapeInterface {

	
	private int b;
	private int h;
	private double area;
	
	public Triangle(int b, int h) {
		this.b=b;
		this.h=h;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

	public int getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}
	
	

	@Override
	public double calculateArea() {
		area = (b*h)/2;
		return area;
	}

	@Override
	public void printInfo() {
		System.out.println("The area of Square is: " + area);
		System.out.println("The Color of Shape is: ");
		System.out.println("The Dimension of the shape is: ");
	}
	
}
