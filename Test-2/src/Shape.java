import java.util.ArrayList;
import java.util.Collection;

public class Shape {

	private ArrayList<Shape> shape = new ArrayList<>();

	public void addShape(Shape shape) {
		this.shape.add(shape);

	}

	public ArrayList<Shape> getShape() {
		return shape;
	}

	public void setShape(ArrayList<Shape> shape) {
		this.shape = shape;
	}

}
