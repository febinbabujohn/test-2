
public class Square extends Shape implements TwoDimensionalShapeInterface {

	private int s;
	private int area;
	
	public Square(int s)
	{
		 this.s = s;
	}

	public int getS() {
		return s;
	}

	public void setS(int s) {
		this.s = s;
	}
	
	

	@Override
	public double calculateArea() {
		
		area = s*s;
		return area;
	}

	@Override
	public void printInfo() {
		// TODO Auto-generated method stub
		System.out.println("The area of Square is: " + area);
		System.out.println("The Color of Shape is: ");
		System.out.println("The Dimension of the shape is: ");
	}
	
}
