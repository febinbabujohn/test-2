// Name: Febin Babu John, ID: C0753814 

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	private static final String Triangle = null;

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int choice = 0;
		
		while (choice != 3) {
			// 1. show the menu
			showMenu();
	
			// 2. get the user input
			System.out.println("Enter a number: ");
			choice = keyboard.nextInt();
			
			// 3. DEBUG: Output what the user typed in 
			System.out.println("You entered: " + choice);
			System.out.println();
			if(choice==1)
			{
				
				System.out.println("Enter the Base of triangle");
				int base = keyboard.nextInt();
				System.out.println("Enter the height of triangle");
				int height = keyboard.nextInt();
				System.out.println("Enter the Color of triangle");
				String color = keyboard.next();
				Triangle t =new Triangle(base,height);
				
				double calcarea= t.calculateArea();
				t.printInfo();
				
				
				
			}
			else if(choice==2)
			{
				System.out.println("Enter the Side of square");
				int side = keyboard.nextInt();
				System.out.println("Enter the Color of triangle");
				String color1 = keyboard.next();
				Square sq = new Square(side);
				sq.calculateArea();
				sq.printInfo();
				
			}
			else if (choice==3)
			{
				System.out.println("Exiting");
				break;
			}
			else
			{
				System.out.println("Please enter a valid option");
			}

		}
	}
	
	public static void showMenu() {
		System.out.println("AREA GENERATOR");
		System.out.println("==============");
		System.out.println("1. Triangle");
		System.out.println("2. Square");
		System.out.println("3. Exit");
	}

}
